#!/bin/bash

str_num=$(ls | grep ^SIM | tail -n 1 | cut -d "-" -f 1 | cut -d "M" -f 2)
let str_num++
str_num_next=$(printf "%05d\n" ${str_num})
next_name=SIM${str_num_next}-${1}
mkdir ${next_name}
cd ${next_name}
echo "clean()" >> test.cfg
echo "compile(${2})" >> test.cfg
echo "run(${2})" >> test.cfg
echo "#include <stdio.h>" >> ${2}.c
echo "" >> ${2}.c
echo "int main(){" >> ${2}.c
echo "" >> ${2}.c
echo "    return 0;" >> ${2}.c
echo "}" >> ${2}.c
