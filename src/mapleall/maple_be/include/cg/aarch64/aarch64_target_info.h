/*
 * Copyright (c) [2022] Huawei Technologies Co.,Ltd.All rights reserved.
 *
 * OpenArkCompiler is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR
 * FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
#ifndef MAPLEBE_INCLUDE_CG_AARCH64_AARCH64_TARGET_INFO_H
#define MAPLEBE_INCLUDE_CG_AARCH64_AARCH64_TARGET_INFO_H
#include "target_info.h"
#include "aarch64_operand.h"
#include "aarch64_insn.h"
#include "aarch64_abi.h"

namespace maplebe {
constexpr size_t kMaxMoveBytes = 8;
class AArch64TargetInfo : public TargetInfo {
 public:
  AArch64TargetInfo() : TargetInfo() {}

  ~AArch64TargetInfo() override = default;
  size_t GetMaxMoveBytes() override { return kMaxMoveBytes; }
  size_t GetIntrinsicInsnNum(MIRIntrinsicID id) override;
};
}  /* namespace maplebe */

#endif  /* MAPLEBE_INCLUDE_CG_AARCH64_AARCH64_TARGET_INFO_H */
