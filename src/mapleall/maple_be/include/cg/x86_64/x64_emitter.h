/*
 * Copyright (c) [2022] Huawei Technologies Co.,Ltd.All rights reserved.
 *
 * OpenArkCompiler is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR
 * FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
#ifndef MAPLEBE_INCLUDE_CG_X86_64_EMITTER_H
#define MAPLEBE_INCLUDE_CG_X86_64_EMITTER_H

#include "asm_emit.h"
#include "visitor_common.h"
#include "operand.h"

namespace maplebe {

class X64Emitter : public AsmEmitter {
 public:
  X64Emitter(CG &cg, const std::string &asmFileName) : AsmEmitter(cg, asmFileName) {}
  ~X64Emitter() = default;

  void EmitRefToMethodDesc(FuncEmitInfo &funcEmitInfo, Emitter &emitter) override;
  void EmitRefToMethodInfo(FuncEmitInfo &funcEmitInfo, Emitter &emitter) override;
  void EmitMethodDesc(FuncEmitInfo &funcEmitInfo, Emitter &emitter) override;
  void EmitFastLSDA(FuncEmitInfo &funcEmitInfo) override;
  void EmitFullLSDA(FuncEmitInfo &funcEmitInfo) override;
  void EmitBBHeaderLabel(FuncEmitInfo &funcEmitInfo, const std::string &name, LabelIdx labIdx) override;
  void EmitJavaInsnAddr(FuncEmitInfo &funcEmitInfo) override;
  void Run(FuncEmitInfo &funcEmitInfo) override;
};

class CGOpndEmitVisitor : public OperandVisitorBase,
                          public OperandVisitors<CGRegOperand, CGImmOperand, CGMemOperand,
                                                 CGFuncNameOperand, CGLabelOperand> {
 public:
  CGOpndEmitVisitor(Emitter &asmEmitter): emitter(asmEmitter) {}
  virtual ~CGOpndEmitVisitor() = default;

 protected:
  Emitter &emitter;
};

class X64OpndEmitVisitor : public CGOpndEmitVisitor {
 public:
  X64OpndEmitVisitor(Emitter &emitter) : CGOpndEmitVisitor(emitter) {}
  ~X64OpndEmitVisitor() override = default;

  void Visit(CGRegOperand *v) final;
  void Visit(CGImmOperand *v) final;
  void Visit(CGMemOperand *v) final;
  void Visit(CGFuncNameOperand *v) final;
  void Visit(CGLabelOperand *v) final;
};

}  /* namespace maplebe */

#endif /* MAPLEBE_INCLUDE_CG_X86_64_EMITTER_H */
