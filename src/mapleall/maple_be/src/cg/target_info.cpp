/*
 * Copyright (c) [2022] Huawei Technologies Co.,Ltd.All rights reserved.
 *
 * OpenArkCompiler is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR
 * FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
#include "mempool.h"
#include "reg_alloc.h"
#include "live.h"
#include "loop.h"
#include "cg_dominance.h"
#include "mir_lower.h"
#include "securec.h"
#include "reg_alloc_basic.h"
#include "cg.h"
#if TARGAARCH64
#include "aarch64_cg.h"
#include "aarch64_cgfunc.h"
#include "aarch64_target_info.h"
#endif

namespace maple {
using namespace maplebe;
TargetInfo *TargetInfo::CreateTargetInfo(MapleAllocator &alloc, BackEnd backEnd) {
  // now we use macro to choose target.
  (void)backEnd;
  TargetInfo *ret = nullptr;
#if TARGAARCH64 || TARGRISCV64
  ret = alloc.New<AArch64TargetInfo>();
#elif TARGARM32
  ret = alloc.New<Arm32TargetInfo>();
#elif TARGX86_64
  ret = alloc.New<X86_64TargetInfo>();
#else
#error "unknown platform"
#endif
  return ret;
}
}  /* namespace maple */
