/*
 * Copyright (c) [2022] Huawei Technologies Co.,Ltd.All rights reserved.
 *
 * OpenArkCompiler is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR
 * FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

#include "x64_cgfunc.h"
#include "x64_cg.h"
#include "x64_emitter.h"
#include "insn.h"

namespace maplebe {
void X64Emitter::EmitRefToMethodDesc(FuncEmitInfo &funcEmitInfo, Emitter &emitter) {}
void X64Emitter::EmitRefToMethodInfo(FuncEmitInfo &funcEmitInfo, Emitter &emitter) {}
void X64Emitter::EmitMethodDesc(FuncEmitInfo &funcEmitInfo, Emitter &emitter) {}
void X64Emitter::EmitFastLSDA(FuncEmitInfo &funcEmitInfo) {}
void X64Emitter::EmitFullLSDA(FuncEmitInfo &funcEmitInfo) {}
void X64Emitter::EmitJavaInsnAddr(FuncEmitInfo &funcEmitInfo) {}

void X64Emitter::EmitBBHeaderLabel(FuncEmitInfo &funcEmitInfo, const std::string &name, LabelIdx labIdx) {
  CGFunc &cgFunc = funcEmitInfo.GetCGFunc();
  CG *currCG = cgFunc.GetCG();
  Emitter &emitter = *(currCG->GetEmitter());

  PUIdx pIdx = currCG->GetMIRModule()->CurFunction()->GetPuidx();
  const char *puIdx = strdup(std::to_string(pIdx).c_str());
  const std::string &labelName = cgFunc.GetFunction().GetLabelTab()->GetName(labIdx);
  if (currCG->GenerateVerboseCG()) {
    emitter.Emit(".L.").Emit(puIdx).Emit("__").Emit(labIdx).Emit(":\t");
    if (!labelName.empty() && labelName.at(0) != '@') {
      /* If label name has @ as its first char, it is not from MIR */
      emitter.Emit("//  MIR: @").Emit(labelName).Emit("\n");
    } else {
      emitter.Emit("\n");
    }
  } else {
    emitter.Emit(".L.").Emit(puIdx).Emit("__").Emit(labIdx).Emit(":\n");
  }
}

void X64OpndEmitVisitor::Visit(maplebe::CGRegOperand *v) {
  /* Mapping with physical register after register allocation is done
   * try table-driven register mapping ? */
  uint8 regType = -1;
  switch (v->GetSize()) {
    case k8BitSize:
      /* use lower 8-bits */
      regType = X64CG::kR8LowList;
      break;
    case k16BitSize:
      regType = X64CG::kR16List;
      break;
    case k32BitSize:
      regType = X64CG::kR32List;
      break;
    case k64BitSize:
      regType = X64CG::kR64List;
      break;
    default:
      CHECK_FATAL(false, "unkown reg size");
      break;
  }
  emitter.Emit("%").Emit(X64CG::intRegNames[regType][v->GetRegisterNumber()]);
}
void X64OpndEmitVisitor::Visit(maplebe::CGImmOperand *v) {
  if (v->GetSymbol() != nullptr && v->GetSize() == 0) {
    /* symbol form imm */
    emitter.Emit(v->GetName());
  } else {
    /* general imm */
    emitter.Emit("$").Emit(v->GetValue());
  }
  return;
}
void X64OpndEmitVisitor::Visit(maplebe::CGMemOperand *v) {
  if (v->GetBaseOfst() != nullptr) {
    if (v->GetBaseOfst()->GetSymbol() != nullptr && v->GetBaseOfst()->GetSize() == 0) {
      /* symbol form offset */
      emitter.Emit(v->GetBaseOfst()->GetName());
    } else {
      /* general offset */
      emitter.Emit(v->GetBaseOfst()->GetValue());
    }
  }
  emitter.Emit("(");
  if (v->GetBaseRegister() != nullptr) {
    /* Emit RBP or EBP only when index register doesn't exist */
    if ((v->GetIndexRegister() != nullptr && v->GetBaseRegister()->GetRegisterNumber() != x64::RBP) ||
        v->GetIndexRegister() == nullptr) {
      Visit(v->GetBaseRegister());
    }
  }
  if (v->GetIndexRegister() != nullptr) {
    emitter.Emit(", ");
    Visit(v->GetIndexRegister());
    emitter.Emit(", ").Emit(v->GetScaleFactor()->GetValue());
  }
  emitter.Emit(")");
}
void X64OpndEmitVisitor::Visit(maplebe::CGLabelOperand *v) {
  std::string labelName = v->GetParentFunc();
  /* If this label indicates a bb's addr (named as: ".L." + UniqueID + "__" + Offset),
   * prefix "$" is not required. */
  if (!labelName.empty() && labelName[0] != '.') {
    emitter.Emit("$");
  }
  emitter.Emit(labelName);
}
void X64OpndEmitVisitor::Visit(maplebe::CGFuncNameOperand *v) {
  emitter.Emit(v->GetName());
}

void DumpTargetASM(Emitter &emitter, Insn &insn) {
  emitter.Emit("\t");
  const InsnDescription &curMd = X64CG::kMd[insn.GetMachineOpcode()];
  emitter.Emit(curMd.GetName()).Emit("\t");
  /* In AT&T assembly syntax, Indirect jump/call operands are indicated
   * with asterisk "*" (as opposed to direct).
   * Direct jump/call: jmp .L.xxx__x or callq funcName
   * Indirect jump/call: jmp *%rax; jmp *(%rax) or callq *%rax; callq *(%rax)
   */
  if (curMd.IsCall() || curMd.IsCondBranch()) {
    const OpndDescription* opndDesc = curMd.GetOpndDes(0);
    if (opndDesc->IsRegister() || opndDesc->IsMem()) {
      emitter.Emit("*");
    }
  }
  /* Get Operands Number */
  size_t size = 0;
  std::string format(curMd.format);
  for (char c : format) {
    if (c != ',') {
      size = size + 1;
    }
  }

  for (int i = 0; i < size; i++) {
    Operand *opnd = &insn.GetOperand(i);
    X64OpndEmitVisitor visitor(emitter);
    opnd->Accept(visitor);
    if (i != size - 1) {
      emitter.Emit(",\t");
    }
  }
  emitter.Emit("\n");
}

void EmitFunctionHeader(FuncEmitInfo &funcEmitInfo) {
  CGFunc &cgFunc = funcEmitInfo.GetCGFunc();
  CG *currCG = cgFunc.GetCG();
  const MIRSymbol *funcSymbol = cgFunc.GetFunction().GetFuncSymbol();
  Emitter &emitter = *currCG->GetEmitter();

  if (cgFunc.GetFunction().GetAttr(FUNCATTR_section)) {
    const std::string &sectionName = cgFunc.GetFunction().GetAttrs().GetPrefixSectionName();
    (void)emitter.Emit("\t.section  " + sectionName).Emit(",\"ax\",@progbits\n");
  } else {
    emitter.EmitAsmLabel(kAsmText);
  }
  emitter.EmitAsmLabel(*funcSymbol, kAsmAlign);

  if (funcSymbol->GetFunction()->GetAttr(FUNCATTR_weak)) {
    emitter.EmitAsmLabel(*funcSymbol, kAsmWeak);
    emitter.EmitAsmLabel(*funcSymbol, kAsmHidden);
  } else if (funcSymbol->GetFunction()->GetAttr(FUNCATTR_local)) {
    emitter.EmitAsmLabel(*funcSymbol, kAsmLocal);
  } else {
    emitter.EmitAsmLabel(*funcSymbol, kAsmGlbl);
    if (!currCG->GetMIRModule()->IsCModule()) {
      emitter.EmitAsmLabel(*funcSymbol, kAsmHidden);
    }
  }
  emitter.EmitAsmLabel(kAsmType);
  emitter.Emit(funcSymbol->GetName()).Emit(", %function\n");
  emitter.EmitAsmLabel(*funcSymbol, kAsmSyname);
}

void X64Emitter::Run(FuncEmitInfo &funcEmitInfo) {
  CGFunc &cgFunc = funcEmitInfo.GetCGFunc();
  X64CGFunc &x64CGFunc = static_cast<X64CGFunc&>(cgFunc);
  CG *currCG = cgFunc.GetCG();
  const MIRSymbol *funcSymbol = cgFunc.GetFunction().GetFuncSymbol();
  Emitter &emitter = *currCG->GetEmitter();
  /* emit function header */
  EmitFunctionHeader(funcEmitInfo);

  /* emit instructions */
  const std::string &funcName = std::string(cgFunc.GetShortFuncName().c_str());
  FOR_ALL_BB(bb, &x64CGFunc) {
    if (bb->IsUnreachable()) {
      continue;
    }
    if (currCG->GenerateVerboseCG()) {
      emitter.Emit("//    freq:").Emit(bb->GetFrequency()).Emit("\n");
    }
    /* emit bb headers */
    if (bb->GetLabIdx() != MIRLabelTable::GetDummyLabel()) {
      EmitBBHeaderLabel(funcEmitInfo, funcName, bb->GetLabIdx());
    }

    FOR_BB_INSNS(insn, bb) {
      DumpTargetASM(emitter, *insn);
    }
  }
  /* Specially, emit switch table here */
  for (auto &it : cgFunc.GetEmitStVec()) {
    MIRSymbol *st = it.second;
    ASSERT(st->IsReadOnly(), "NYI");
    emitter.Emit("\n");
    emitter.Emit("\t.align 8\n");
    emitter.Emit(st->GetName() + ":\n");
    MIRAggConst *arrayConst = safe_cast<MIRAggConst>(st->GetKonst());
    CHECK_FATAL(arrayConst != nullptr, "null ptr check");
    PUIdx pIdx = cgFunc.GetMirModule().CurFunction()->GetPuidx();
    const std::string &idx = strdup(std::to_string(pIdx).c_str());
    for (size_t i = 0; i < arrayConst->GetConstVec().size(); i++) {
      MIRLblConst *lblConst = safe_cast<MIRLblConst>(arrayConst->GetConstVecItem(i));
      CHECK_FATAL(lblConst != nullptr, "null ptr check");
      (void)emitter.Emit("\t.quad\t.L." + idx).Emit("__").Emit(lblConst->GetValue());
      (void)emitter.Emit("\n");
    }
    (void)emitter.Emit("\n");
  }
  emitter.EmitAsmLabel(*funcSymbol, kAsmSize);
}

bool CgEmission::PhaseRun(maplebe::CGFunc &f) {
  Emitter *emitter = f.GetCG()->GetEmitter();
  CHECK_NULL_FATAL(emitter);
  AsmFuncEmitInfo funcEmitInfo(f);
  emitter->EmitLocalVariable(f);
  static_cast<X64Emitter*>(emitter)->Run(funcEmitInfo);
  return false;
}
MAPLE_TRANSFORM_PHASE_REGISTER(CgEmission, cgemit)
}  /* namespace maplebe */
