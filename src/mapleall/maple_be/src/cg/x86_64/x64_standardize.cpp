/*
 * Copyright (c) [2022] Huawei Technologies Co.,Ltd.All rights reserved.
 *
 * OpenArkCompiler is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR
 * FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

#include "x64_standardize.h"
#include "x64_isa.h"
#include "x64_cg.h"
#include "insn.h"

namespace maplebe {
#define DEFINE_MAPPING(ABSTRACT_IR, X64_MOP, ...) {ABSTRACT_IR, X64_MOP},
std::unordered_map<MOperator, X64MOP_t> x64AbstractMapping = {
#include "x64_abstract_mapping.def"
};

static inline X64MOP_t GetMopFromAbstraceIRMop(MOperator mOp) {
  auto iter = x64AbstractMapping.find(mOp);
  if (iter == x64AbstractMapping.end()) {
    CHECK_FATAL(false, "NIY mapping");
  }
  CHECK_FATAL(iter->second != x64::MOP_begin, "NIY mapping");
  return iter->second;
}

bool X64Standardize::TryFastTargetIRMapping(maplebe::Insn &insn) {
  const InsnDescription &targetMd = X64CG::kMd[insn.GetMachineOpcode()];
  auto cmpFunc = [](const InsnDescription &left, const InsnDescription &right)->bool {
    uint32 leftPropClear = left.properties | ISABSTRACT;
    uint32 rightPropClear = right.properties | ISABSTRACT;
    if ((left.opndMD.size() == right.opndMD.size()) && (leftPropClear == rightPropClear)) {
      for (size_t i = 0; i < left.opndMD.size(); ++i) {
        if (left.opndMD[i] != right.opndMD[i]) {
          return false;
        }
      }
      if (CGOptions::kVerboseAsm) {
        LogInfo::MapleLogger() << "Do fast mapping for " << left.name << " to " << right.name << " successfully\n";
      }
      return true;
    }
    return false;
  };
  if (insn.GetInsnDescrption()->IsSame(targetMd, cmpFunc)) {
    insn.SetMOP(targetMd);
    return true;
  }
  return false;
}

void X64Standardize::StdzMov(maplebe::Insn &insn) {
  X64MOP_t directlyMappingMop = GetMopFromAbstraceIRMop(insn.GetMachineOpcode());
  insn.SetMOP(X64CG::kMd[directlyMappingMop]);
  insn.CommuteOperands(kInsnFirstOpnd, kInsnSecondOpnd);
}

void X64Standardize::StdzStrLdr(Insn &insn) {
  StdzMov(insn);
}

void X64Standardize::StdzBasicOp(Insn &insn) {
  X64MOP_t directlyMappingMop = GetMopFromAbstraceIRMop(insn.GetMachineOpcode());
  insn.SetMOP(X64CG::kMd[directlyMappingMop]);
  Operand &dest = insn.GetOperand(kInsnFirstOpnd);
  Operand &src2 = insn.GetOperand(kInsnThirdOpnd);
  insn.CleanAllOperand();
  insn.AddOperandChain(src2).AddOperandChain(dest);
}

void X64Standardize::StdzUnaryOp(Insn &insn) {
  MOperator directlyMappingMop = abstract::MOP_undef;
  switch (insn.GetMachineOpcode()) {
    case abstract::MOP_neg_32:
      directlyMappingMop = x64::MOP_negl_r;
      break;
    case abstract::MOP_not_64:
      directlyMappingMop = x64::MOP_notq_r;
      break;
    case abstract::MOP_not_32:
      directlyMappingMop = x64::MOP_notl_r;
      break;
    default:
      break;
  }
  if (directlyMappingMop != abstract::MOP_undef) {
    insn.SetMOP(X64CG::kMd[directlyMappingMop]);
    Operand &dest = insn.GetOperand(kInsnFirstOpnd);
    insn.CleanAllOperand();
    insn.AddOperandChain(dest);
  } else {
    CHECK_FATAL(false, "NIY mapping");
  }
}

void X64Standardize::StdzCvtOp(Insn &insn, CGFunc &cgFunc) {
  MOperator directlyMappingMop = abstract::MOP_undef;
  uint32 OpndDesSize = insn.GetInsnDescrption()->GetOpndDes(kInsnFirstOpnd)->GetSize();
  uint32 destSize = OpndDesSize;
  uint32 OpndSrcSize = insn.GetInsnDescrption()->GetOpndDes(kInsnSecondOpnd)->GetSize();
  uint32 srcSize = OpndSrcSize;
  switch (insn.GetMachineOpcode()) {
    case abstract::MOP_zext_rr_32_8:
      directlyMappingMop = x64::MOP_movzbl_r_r;
      break;
    case abstract::MOP_sext_rr_32_8:
      directlyMappingMop = x64::MOP_movsbl_r_r;
      break;
    case abstract::MOP_zext_rr_32_16:
      directlyMappingMop = x64::MOP_movzwl_r_r;
      break;
    case abstract::MOP_sext_rr_32_16:
      directlyMappingMop = x64::MOP_movswl_r_r;
      break;
    case abstract::MOP_zext_rr_64_8:
      destSize = k32BitSize;
      directlyMappingMop = x64::MOP_movzbl_r_r;
      break;
    case abstract::MOP_sext_rr_64_8:
      directlyMappingMop = x64::MOP_movsbq_r_r;
      break;
    case abstract::MOP_zext_rr_64_16:
      destSize = k32BitSize;
      directlyMappingMop = x64::MOP_movzwl_r_r;
      break;
    case abstract::MOP_sext_rr_64_16:
      directlyMappingMop = x64::MOP_movswq_r_r;
      break;
    case abstract::MOP_zext_rr_64_32:
      destSize = k32BitSize;
      directlyMappingMop = x64::MOP_movl_r_r;
      break;
    case abstract::MOP_sext_rr_64_32:
      directlyMappingMop = MOP_movslq_r_r;
      break;
    case abstract::MOP_zext_ri_64_32:
      destSize = k32BitSize;
      directlyMappingMop = x64::MOP_movl_i_r;
      break;
    case abstract::MOP_sext_ri_64_32:
      directlyMappingMop = MOP_movslq_i_r;
      break;
    case abstract::MOP_trunc_rr_8_16:
    case abstract::MOP_trunc_rr_8_32:
    case abstract::MOP_trunc_rr_8_64:
      srcSize = k8BitSize;
      directlyMappingMop = x64::MOP_movb_r_r;
      break;
    case abstract::MOP_trunc_ri_8_16:
    case abstract::MOP_trunc_ri_8_32:
    case abstract::MOP_trunc_ri_8_64:
      srcSize = k8BitSize;
      directlyMappingMop = x64::MOP_movb_i_r;
      break;
    case abstract::MOP_trunc_rr_16_32:
    case abstract::MOP_trunc_rr_16_64:
      srcSize = k16BitSize;
      directlyMappingMop = x64::MOP_movw_r_r;
      break;
    case abstract::MOP_trunc_ri_16_64:
    case abstract::MOP_trunc_ri_16_32:
      srcSize = k16BitSize;
      directlyMappingMop = x64::MOP_movw_i_r;
      break;
    case abstract::MOP_trunc_rr_32_64:
      srcSize = k32BitSize;
      directlyMappingMop = x64::MOP_movl_r_r;
      break;
    case abstract::MOP_trunc_ri_32_64:
      srcSize = k32BitSize;
      directlyMappingMop = x64::MOP_movl_i_r;
      break;
    default:
      break;
  }
  if (directlyMappingMop != abstract::MOP_undef) {
    insn.SetMOP(X64CG::kMd[directlyMappingMop]);
    Operand *opnd0 = &insn.GetOperand(kInsnSecondOpnd);
    CGRegOperand *src = static_cast<CGRegOperand*>(opnd0);
    if (srcSize != OpndSrcSize) {
      src = &cgFunc.GetOpndBuilder()->CreateVReg(src->GetRegisterNumber(),
          srcSize, src->GetRegisterType());
    }
    Operand *opnd1 = &insn.GetOperand(kInsnFirstOpnd);
    CGRegOperand *dest = static_cast<CGRegOperand*>(opnd1);
    if (destSize != OpndDesSize) {
      dest = &cgFunc.GetOpndBuilder()->CreateVReg(dest->GetRegisterNumber(),
          destSize, dest->GetRegisterType());
    }
    insn.CleanAllOperand();
    insn.AddOperandChain(*src).AddOperandChain(*dest);
  } else {
    CHECK_FATAL(false, "NIY mapping");
  }
}

void X64Standardize::StdzShiftOp(Insn &insn, CGFunc &cgFunc) {
  CGRegOperand *countOpnd = static_cast<CGRegOperand*>(&insn.GetOperand(kInsnThirdOpnd));
  /* count operand cvt -> PTY_u8 */
  if (countOpnd->GetSize() != GetPrimTypeBitSize(PTY_u8)) {
    countOpnd = &cgFunc.GetOpndBuilder()->CreateVReg(countOpnd->GetRegisterNumber(),
        GetPrimTypeBitSize(PTY_u8), countOpnd->GetRegisterType());
  }
  /* copy count operand to cl(rcx) register */
  CGRegOperand &clOpnd = cgFunc.GetOpndBuilder()->CreatePReg(x64::RCX, GetPrimTypeBitSize(PTY_u8), kRegTyInt);
  X64MOP_t copyMop = x64::MOP_movb_r_r;
  Insn &copyInsn = cgFunc.GetInsnBuilder()->BuildInsn(copyMop, X64CG::kMd[copyMop]);
  copyInsn.AddOperandChain(*countOpnd).AddOperandChain(clOpnd);
  cgFunc.GetCurBB()->InsertInsnBefore(insn, copyInsn);
  /* shift OP */
  X64MOP_t directlyMappingMop = GetMopFromAbstraceIRMop(insn.GetMachineOpcode());
  insn.SetMOP(X64CG::kMd[directlyMappingMop]);
  CGRegOperand &destOpnd = static_cast<CGRegOperand&>(insn.GetOperand(kInsnFirstOpnd));
  insn.CleanAllOperand();
  insn.AddOperandChain(clOpnd).AddOperandChain(destOpnd);
}

}
