/*
 * Copyright (c) [2022] Huawei Technologies Co.,Ltd.All rights reserved.
 *
 * OpenArkCompiler is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR
 * FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
#include "x64_cgfunc.h"
#include "becommon.h"
#include "abi.h"
#include "x64_call_conv.h"

namespace maplebe {
using namespace maple;
using namespace x64;
constexpr int kMaxRegCount = 4;

/* X86-64 Function Calling Sequence */
int32 ProcessStructWhenClassifyAggregate(MIRStructType &structType, ArgumentClass classes[kMaxRegCount],
                                         uint64 sizeOfTy) {
  uint32 accSize = 0;
  uint32 numRegs = 0;

  for (uint32 f = 0; f < structType.GetFieldsSize(); ++f) {
    TyIdx fieldTyIdx = structType.GetFieldsElemt(f).second.first;
    MIRType *fieldType = GlobalTables::GetTypeTable().GetTypeFromTyIdx(fieldTyIdx);
    if (fieldType->GetKind() == kTypeArray || fieldType->GetKind() == kTypeStruct) {
      CHECK_FATAL(false, "NYI");
      return 0;
    }
    PrimType eleType = fieldType->GetPrimType();
    if (IsPrimitivePureScalar(eleType)) {
      uint32 curSize = GetPrimTypeSize(eleType);
      if (accSize + curSize > k1EightBytesSize) {
        numRegs++;
        accSize = curSize;
        continue;
      }
      accSize += curSize;
    } else {
      CHECK_FATAL(false, "NYI");
      return 0;
    }
  }
  /* Reg Usage after the last element is processed */
  numRegs += (RoundUp(accSize, k8ByteSize) >> k8BitShift);
  if (numRegs > kTwoRegister) {
    classes[0] = kMemoryClass;
    return static_cast<int32>(sizeOfTy);
  }
  classes[0] = kIntegerClass;
  if (numRegs == kTwoRegister) {
    classes[1] = kIntegerClass;
  }
  return numRegs;
}

int32 ClassifyAggregate(MIRType &mirType, uint64 sizeOfTy, ArgumentClass classes[kMaxRegCount]) {
  /*
   * 1. If the size of an object is larger than four eightbytes, or it contains unaligned
   * fields, it has class MEMORY;
   * 2. for the processors that do not support the __m256 type, if the size of an object
   * is larger than two eightbytes and the first eightbyte is not SSE or any other eightbyte
   * is not SSEUP, it still has class MEMORY.
   * This in turn ensures that for rocessors that do support the __m256 type, if the size of
   * an object is four eightbytes and the first eightbyte is SSE and all other eightbytes are
   * SSEUP, it can be passed in a register.
   *(Currently, assume that m256 is not supported)
   */
  if ((sizeOfTy > k2EightBytesSize)) {
    classes[0] = kMemoryClass;
    return static_cast<int32>(sizeOfTy);
  }
  if (mirType.GetKind() == kTypeStruct) {
    MIRStructType &structType = static_cast<MIRStructType&>(mirType);
    return ProcessStructWhenClassifyAggregate(structType, classes, sizeOfTy);
  }
  CHECK_FATAL(false, "NYI");
  return static_cast<int32>(sizeOfTy);
}

int32 Classification(const BECommon &be, MIRType &mirType, ArgumentClass classes[kMaxRegCount]) {
  switch (mirType.GetPrimType()) {
    /*
     * Arguments of types void, (signed and unsigned) _Bool, char, short, int,
     * long, long long, and pointers are in the INTEGER class.
     */
    case PTY_void:
    case PTY_u1:
    case PTY_u8:
    case PTY_i8:
    case PTY_u16:
    case PTY_i16:
    case PTY_a32:
    case PTY_u32:
    case PTY_i32:
    case PTY_a64:
    case PTY_ptr:
    case PTY_ref:
    case PTY_u64:
    case PTY_i64:
      classes[0] = kIntegerClass;
      return kOneRegister;
    /*
     * Arguments of type __int128 offer the same operations as INTEGERs,
     * yet they do not fit into one general purpose register but require
     * two registers.
     */
    case PTY_i128:
    case PTY_u128:
      classes[0] = kIntegerClass;
      classes[1] = kIntegerClass;
      return kTwoRegister;
    case PTY_agg: {
      /*
       * The size of each argument gets rounded up to eightbytes,
       * Therefore the stack will always be eightbyte aligned.
       */
      uint64 sizeOfTy = RoundUp(be.GetTypeSize(mirType.GetTypeIndex()), k8ByteSize);
      CHECK_FATAL(sizeOfTy != 0, "NIY");
      /* If the size of an object is larger than four eightbytes, it has class MEMORY */
      if ((sizeOfTy > k4EightBytesSize)) {
          classes[0] = kMemoryClass;
          return static_cast<int32>(sizeOfTy);
      }
      return ClassifyAggregate(mirType, sizeOfTy, classes);
    }
    default:
      CHECK_FATAL(false, "NYI");
  }
  return 0;
}

void X64CallConvImpl::InitCCLocInfo(CCLocInfo &pLoc) const {
  pLoc.reg0 = kRinvalid;
  pLoc.reg1 = kRinvalid;
  pLoc.reg2 = kRinvalid;
  pLoc.reg3 = kRinvalid;
  pLoc.memOffset = nextStackArgAdress;
  pLoc.fpSize = 0;
  pLoc.numFpPureRegs = 0;
}

int32 X64CallConvImpl::LocateNextParm(MIRType &mirType, CCLocInfo &pLoc, bool isFirst, MIRFunction *tFunc) {
  InitCCLocInfo(pLoc);
  ArgumentClass classes[kMaxRegCount] = { kNoClass };  /* Max of four Regs. */
  int32 alignedTySize = Classification(beCommon, mirType, classes);
  if (alignedTySize == 0) {
    return 0;
  }
  pLoc.memSize = alignedTySize;
  ++paramNum;
  if (classes[0] == kIntegerClass) {
    if (alignedTySize == kOneRegister) {
      pLoc.reg0 = AllocateGPParmRegister();
      ASSERT(nextGeneralParmRegNO <= kNumIntParmRegs, "RegNo should be pramRegNO");
    } else if (alignedTySize == kTwoRegister) {
      AllocateTwoGPParmRegisters(pLoc);
      ASSERT(nextGeneralParmRegNO <= kNumIntParmRegs, "RegNo should be pramRegNO");
    }
  }
  if (pLoc.reg0 == kRinvalid || classes[0] == kMemoryClass) {
    /* being passed in memory */
    nextStackArgAdress = pLoc.memOffset + alignedTySize * k8ByteSize;
  }
  return 0;
}

int32 X64CallConvImpl::LocateRetVal(MIRType &retType, CCLocInfo &pLoc) {
  InitCCLocInfo(pLoc);
  ArgumentClass classes[kMaxRegCount] = { kNoClass };  /* Max of four Regs. */
  int32 alignedTySize = Classification(beCommon, retType, classes);
  if (alignedTySize == 0) {
    return 0;    /* size 0 ret val */
  }
  if (classes[0] == kIntegerClass) {
    /* If the class is INTEGER, the next available register of the sequence %rax, */
    /* %rdx is used. */
    CHECK_FATAL(alignedTySize <= kTwoRegister, "LocateRetVal: illegal number of regs");
    pLoc.regCount = alignedTySize;
    if (alignedTySize == kOneRegister) {
      pLoc.reg0 = AllocateGPReturnRegister();
      ASSERT(nextGeneralReturnRegNO <= kNumIntReturnRegs, "RegNo should be pramRegNO");
    } else if (alignedTySize == kTwoRegister) {
      AllocateTwoGPReturnRegisters(pLoc);
      ASSERT(nextGeneralReturnRegNO <= kNumIntReturnRegs, "RegNo should be pramRegNO");
    }
    if (nextGeneralReturnRegNO == kOneRegister) {
      pLoc.primTypeOfReg0 = retType.GetPrimType();
    } else if (nextGeneralReturnRegNO == kTwoRegister) {
      pLoc.primTypeOfReg1 = retType.GetPrimType();
    }
    return 0;
  }
  if (pLoc.reg0 == kRinvalid || classes[0] == kMemoryClass) {
    /*
     * the caller provides space for the return value and passes
     * the address of this storage in %rdi as if it were the first
     * argument to the function. In effect, this address becomes a
     * “hidden” first argument.
     * On return %rax will contain the address that has been passed
     * in by the caller in %rdi.
     * Currently, this scenario is not fully supported.
     */
    pLoc.reg0 = AllocateGPReturnRegister();
    return 0;
  }
  CHECK_FATAL(false, "NYI");
  return 0;
}
}  /* namespace maplebe */
