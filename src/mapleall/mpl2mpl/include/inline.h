/*
 * Copyright (c) [2019-2021] Huawei Technologies Co.,Ltd.All rights reserved.
 *
 * OpenArkCompiler is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR
 * FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
#ifndef MPL2MPL_INCLUDE_INLINE_H
#define MPL2MPL_INCLUDE_INLINE_H
#include "call_graph.h"
#include "inline_transformer.h"
#include "maple_phase_manager.h"
#include "me_option.h"
#include "mempool.h"
#include "mempool_allocator.h"
#include "mir_builder.h"
#include "mir_function.h"
#include "mir_parser.h"
#include "opcode_info.h"
#include "string_utils.h"

namespace maple {

struct InlineResult {
  bool canInline;
  std::string reason;

  InlineResult(bool canInline, const std::string &reason) : canInline(canInline), reason(reason) {}
  ~InlineResult() = default;
};

enum FuncCostResultType {
  kNotAllowedNode,
  kFuncBodyTooBig,
  kSmallFuncBody
};

enum ThresholdType {
  kSmallFuncThreshold,
  kHotFuncThreshold,
  kRecursiveFuncThreshold,
  kHotAndRecursiveFuncThreshold
};

class MInline {
 public:
  MInline(MIRModule &mod, MemPool *memPool, CallGraph *cg = nullptr)
      : alloc(memPool),
        module(mod),
        builder(*mod.GetMIRBuilder()),
        funcToCostMap(alloc.Adapter()),
        cg(cg),
        excludedCaller(alloc.Adapter()),
        excludedCallee(alloc.Adapter()),
        hardCodedCallee(alloc.Adapter()),
        rcWhiteList(alloc.Adapter()),
        inlineListCallee(alloc.Adapter()),
        noInlineListCallee(alloc.Adapter()) {
    Init();
  };
  void Inline();
  void CleanupInline();
  virtual ~MInline() {
    cg = nullptr;
  }

 protected:
  MapleAllocator alloc;
  MIRModule &module;
  MIRBuilder &builder;
  // save the cost of calculated func to reduce the amount of calculation
  MapleMap<MIRFunction*, uint32> funcToCostMap;
  CallGraph *cg;

 private:
  void Init();
  void InitParams();
  void InitProfile() const;
  void InitExcludedCaller();
  void InitExcludedCallee();
  void InitRCWhiteList();
  void ApplyInlineListInfo(const std::string &list, MapleMap<GStrIdx, MapleSet<GStrIdx>*> &listCallee);
  FuncCostResultType GetFuncCost(const MIRFunction&, const BaseNode&, uint32&, uint32) const;
  bool FuncInlinable(const MIRFunction&) const;
  bool IsSafeToInline(const MIRFunction*, const CallNode&) const;
  bool IsHotCallSite(const MIRFunction &caller, const MIRFunction &callee, const CallNode &callStmt) const;
  InlineResult AnalyzeCallsite(const MIRFunction &caller, MIRFunction &callee, const CallNode &callStmt);
  InlineResult AnalyzeCallee(const MIRFunction &caller, MIRFunction &callee, const CallNode &callStmt);
  void AdjustInlineThreshold(const MIRFunction &caller, const MIRFunction &callee, const CallNode &callStmt,
      uint32 &threshold, uint32 &thresholdType);
  virtual bool CanInline(CGNode*, std::unordered_map<MIRFunction*, bool>&) {
    return false;
  }

  bool CheckCalleeAndInline(MIRFunction*, BlockNode *enclosingBlk, CallNode*, MIRFunction*);
  bool SuitableForTailCallOpt(BaseNode &enclosingBlk, const StmtNode &stmtNode, CallNode &callStmt);
  bool CalleeReturnValueCheck(StmtNode &stmtNode, CallNode &callStmt);
  void InlineCalls(CGNode&);
  void PostInline(MIRFunction&);
  void InlineCallsBlock(MIRFunction&, BlockNode&, BaseNode&, bool&, BaseNode&);
  void InlineCallsBlockInternal(MIRFunction&, BlockNode&, BaseNode&, bool&);
  GotoNode *UpdateReturnStmts(const MIRFunction&, BlockNode&, LabelIdx, const CallReturnVector&, int&) const;
  void CollectMustInlineFuncs();
  void ComputeTotalSize();
  void MarkSymbolUsed(const StIdx&) const;
  void MarkUsedSymbols(const BaseNode*) const;
  void MarkFunctionUsed(MIRFunction *func, bool inlined = false) const;
  void MarkUnInlinableFunction() const;
  bool HasAccessStatic(const BaseNode&) const;
  uint64 totalSize = 0;
  bool dumpDetail = false;
  std::string dumpFunc = "";
  MapleSet<GStrIdx> excludedCaller;
  MapleSet<GStrIdx> excludedCallee;
  MapleSet<GStrIdx> hardCodedCallee;
  MapleSet<GStrIdx> rcWhiteList;
  MapleMap<GStrIdx, MapleSet<GStrIdx>*> inlineListCallee;
  MapleMap<GStrIdx, MapleSet<GStrIdx>*> noInlineListCallee;
  uint32 smallFuncThreshold = 0;
  uint32 hotFuncThreshold = 0;
  uint32 recursiveFuncThreshold = 0;
  bool inlineWithProfile = false;
  std::string inlineFuncList;
  std::string noInlineFuncList;
  uint32 maxRecursiveLevel = 4;  // for recursive function, allow inline 4 levels at most.
};

MAPLE_MODULE_PHASE_DECLARE(M2MInline)
}  // namespace maple
#endif  // MPL2MPL_INCLUDE_INLINE_H
